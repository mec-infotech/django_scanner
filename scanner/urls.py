from django.urls import path
from .views import scan_qr, scanned_data

urlpatterns = [
    path('', scan_qr, name='scan_qr'),
    path('scanned/', scanned_data, name='scanned_data'),
]
