from django.shortcuts import render

def scan_qr(request):
    return render(request, 'scanner/scan_qr.html')

def scanned_data(request):
    data = request.GET.get('data', '')
    return render(request, 'scanner/scanned_data.html', {'data': data})
